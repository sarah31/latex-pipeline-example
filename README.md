This repository demonstrates a Bitbucket continuous integration pipeline that compiles `example.tex` into `example.pdf` and uploads it to the Bitbucket downloads area.

Note: to use this repository yourself, you need to configure an application key to enable the pipeline to load the produced PDF into the downloads area. The application key needs to go into Settings -> (Pipelines) Environment variables -> `BB_AUTH_STRING`.
